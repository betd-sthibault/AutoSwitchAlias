#!/bin/sh
function dotenv () {
    set -a
    [ -f .env ] && . .env
    export uid=$(stat -c %u .)
    export gid=$(stat -c %g .)
set +a
}

function composer () {
    dotenv
    if [ $(pwd) == ${PROJECT_PATH} ]; then
        docker run -it --rm -v $(pwd):/var/www -u ${UID}:www-data  composer $@
    fi
}

