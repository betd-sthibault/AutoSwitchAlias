#!/bin/sh
function dotenv () {
    set -a
    [ -f .env ] && . .env
    export uid=$(stat -c %u .)
    export gid=$(stat -c %g .)
    set +a
}

function composer () {
    dotenv
    if [ $(pwd) == ${PROJECT_PATH} ]; then
        docker pull ppottie/c-pyng:php7.1
        docker run -it --rm -v $HOME/.composer:/.composer -v $(pwd):/var/www -u ${UID}:www-data ppottie/c-pyng composer $@
    fi
}

