#!/bin/sh

function dotenv () {
  set -a
  [ -f .env ] && . .env
  export uid=$(stat -c %u .)
  export gid=$(stat -c %g .)
  dockercomposeYML=""
  if [[  -f docker-compose.dist.yml ]]; then
    dockercomposeYML=" -f docker-compose.dist.yml"
  fi
  if [[  -f docker-compose.${APP_ENV}.yml ]]; then
    dockercomposeYML="${dockercomposeYML} -f docker-compose.${APP_ENV}.yml"
  fi
  if [[ -f docker-compose.yml ]] || [[ $dockercomposeYML ]]; then
    export DC="docker-compose ${dockercomposeYML} exec -u ${uid}:${gid}"
  fi
  set +a
}


function composer () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm composer $@
   fi
}

function console () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm bin/console $@
   fi
}
function behat () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [[ $DC ]]; then
       eval ${DC} api-php-fpm vendor/bin/behat $@
   fi
}

function yarn () {
   dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [ -f docker-compose.${APP_ENV}.yml ]; then
        docker pull ppottie/c-pyng:php7.1
        docker run -it --rm -v $(pwd):/var/www -u  ${uid}:${gid}  ppottie/c-pyng:php7.1  yarn $@
   fi
}
function gulp () {
    dotenv
   if [ $(pwd) == ${PROJECT_PATH} ] && [ -f docker-compose.${APP_ENV}.yml ]; then
    docker pull ppottie/c-pyng:php7.1
       docker run -it --rm -v $(pwd)/var/.yarnrc:/.yarnrc -v $(pwd):/var/www -u  ${uid}:${gid} ppottie/c-pyng:php7.1 gulp $@
   else
        gulp $@
   fi
}
